package uy.edu.cei.dda.rmi.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import uy.edu.cei.dda.rmi.common.server.ControllerFacade;
import uy.edu.cei.dda.rmi.common.server.UserController;

public class ControllerFacadeImpl
		extends UnicastRemoteObject
		implements ControllerFacade {

	/**
	 * Default UID
	 */
	private static final long serialVersionUID = 1L;

	public ControllerFacadeImpl() throws RemoteException {
		super();
	}
	
	@Override
	public UserController getUserController() throws RemoteException {
		return new UserControllerImpl();
	}
}
