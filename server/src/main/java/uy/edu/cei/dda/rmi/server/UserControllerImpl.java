package uy.edu.cei.dda.rmi.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import uy.edu.cei.dda.rmi.common.common.User;
import uy.edu.cei.dda.rmi.common.server.UserController;

public class UserControllerImpl
		extends UnicastRemoteObject
		implements UserController {

	/**
	 * Default UID
	 */
	private static final long serialVersionUID = 1L;

	protected UserControllerImpl() throws RemoteException {
		super();
	}

	@Override
	public User fetchUser(String name) throws RemoteException {
		User user = new User(name);
		System.out.println(user);
		
		return user;
	}
}
