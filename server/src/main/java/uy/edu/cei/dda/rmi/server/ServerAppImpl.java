package uy.edu.cei.dda.rmi.server;

import java.io.Serializable;
import java.rmi.RemoteException;

import uy.edu.cei.dda.rmi.common.common.Message;
import uy.edu.cei.dda.rmi.common.server.ControllerFacade;
import uy.edu.cei.dda.rmi.common.server.ServerApp;

public class ServerAppImpl implements ServerApp, Serializable {

	private String lastMessage;
	private ControllerFacade controllerFacade;
	
	protected ServerAppImpl() throws RemoteException {
		super();
		this.controllerFacade = new ControllerFacadeImpl();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void sayHello(String name) throws RemoteException {
		System.out.println("Hello " + name);
	}

	@Override
	public Message sendMessage(Message message) throws RemoteException {
		System.out.println(message.getMessage());
		System.out.println(this.lastMessage);
		this.lastMessage = message.getMessage();
		System.out.println(this.lastMessage);
		return new Message("hola desde el otro lado");
	}
	
	public ControllerFacade getControllerFacade() throws RemoteException {
		return this.controllerFacade;
	}
	
}
