package uy.edu.cei.dda.rmi.client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import uy.edu.cei.dda.rmi.common.common.Message;
import uy.edu.cei.dda.rmi.common.common.User;
import uy.edu.cei.dda.rmi.common.server.ControllerFacade;
import uy.edu.cei.dda.rmi.common.server.ServerApp;
import uy.edu.cei.dda.rmi.common.server.UserController;

/**
 * Hello world!
 *
 */
public class App {
	
	public static void main(String[] args) throws Exception {
		
        Registry registry = LocateRegistry.getRegistry(1099);
        ServerApp serverApp = (ServerApp) registry.lookup("server");
        serverApp.sayHello("yo");
        Message response = serverApp.sendMessage(new Message("Hola"));
        System.out.println(response.getMessage());
        ControllerFacade controllerFacade = serverApp.getControllerFacade();
        UserController userController = controllerFacade.getUserController();
        User user = userController.fetchUser("foo");
        
        while(true) {
        	Thread.sleep(100);
        }
	}
}
