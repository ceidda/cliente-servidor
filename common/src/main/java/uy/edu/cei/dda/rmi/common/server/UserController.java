package uy.edu.cei.dda.rmi.common.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

import uy.edu.cei.dda.rmi.common.common.User;

public interface UserController extends Remote {

	User fetchUser(String name) throws RemoteException;

}
