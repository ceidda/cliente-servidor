package uy.edu.cei.dda.rmi.common.common;

import java.io.Serializable;

public class User implements Serializable {

	/**
	 * Default UID
	 */
	private static final long serialVersionUID = 1L;
	private String name;

	public User() {
	}

	public User(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
