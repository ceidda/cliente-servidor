package uy.edu.cei.dda.rmi.common.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

import uy.edu.cei.dda.rmi.common.common.Message;

public interface ServerApp extends Remote {

	public void sayHello(String name) throws RemoteException;

	public Message sendMessage(Message message) throws RemoteException;

	public ControllerFacade getControllerFacade() throws RemoteException;
}
