package uy.edu.cei.dda.rmi.common.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ControllerFacade extends Remote {

	UserController getUserController() throws RemoteException;
	
}
