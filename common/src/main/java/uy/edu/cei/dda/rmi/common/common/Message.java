package uy.edu.cei.dda.rmi.common.common;

import java.io.Serializable;
import java.rmi.Remote;

public class Message implements Serializable {

	private String message;

	public Message() {
	}

	public Message(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
